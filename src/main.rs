use bevy::{
    ecs::query::QueryData,
    prelude::*,
};

fn main() {
    App::new()
        .add_plugins(DefaultPlugins.set(ImagePlugin::default_nearest())) // prevents blurry sprites
        .add_systems(Startup, setup)
        .add_systems(Update, (
            keyboard_input_system, 
            update_bums,
            update_thugs,
            update_cops,
            animate_sprite2
        ))
        .run();
}

#[derive(Component, Copy, Clone, PartialEq)]
enum Direction {
    Up,
    Left,
    Down,
    Right
}

impl Direction {
    pub fn index(&self) -> usize {
        *self as usize
    }
}

#[derive(Component)]
struct AnimationIndices {
    first: usize,
    last: usize,
}

#[derive(Component, Deref, DerefMut)]
struct AnimationTimer(Timer);


#[derive(Component, Deref, DerefMut)]
struct MovementTimer(Timer);

#[derive(Component, Deref, DerefMut)]
struct JustMoved(bool);

#[derive(Bundle)]
struct Movement {
    movement_timer: MovementTimer,
    animation_timer: AnimationTimer,
    animation_indices: AnimationIndices,
    dir: Direction,
    just_moved: JustMoved
}

#[derive(Component)]
struct Player {}

#[derive(Component)]
struct Bum {}

#[derive(Component)]
struct Thug {}

#[derive(Component)]
struct Cop {}

pub trait Ai {
    fn update(&self, time: Time, movement: Movement);
}

impl Ai for Bum {
    fn update(&self, time: Time, movement: Movement) {
    }
}


fn setup(
        mut commands: Commands, 
        asset_server: Res<AssetServer>,
        mut texture_atlas_layouts: ResMut<Assets<TextureAtlasLayout>>) {
    
    let player_texture = asset_server.load("sprites/character.png");
    //let thug_texture = asset_server.load("sprites/thug.png");
    let layout = TextureAtlasLayout::from_grid(Vec2::new(64.0, 64.0), 9, 4, None, None);
    let texture_atlas_layout = texture_atlas_layouts.add(layout);
    
    let player = Movement {
        animation_timer: AnimationTimer(Timer::from_seconds(0.1, TimerMode::Repeating)),
        movement_timer: MovementTimer(Timer::from_seconds(0.01, TimerMode::Repeating)),
        animation_indices: AnimationIndices { first: 0, last: 9 },
        dir: Direction::Down,
        just_moved: JustMoved(false)
    };

    commands.spawn(Camera2dBundle::default());
    commands.spawn((
        player,
        Player{},
        SpriteSheetBundle {
            texture: player_texture,
            atlas: TextureAtlas {
                layout: texture_atlas_layout.clone(),
                index: 9 * Direction::Down.index() + 0
            },
            transform: Transform::from_scale(Vec3::splat(3.0)),
            ..default()
        }
    ));


    for n in 0..3 {
        let mut transform = Transform::from_xyz(n as f32 * 5., 0., 0.);
        transform.scale = Vec3::splat(3.0);

        let bum_texture = asset_server.load("sprites/bum.png");
        
        let bum = Movement {
            movement_timer: MovementTimer(Timer::from_seconds(0.1, TimerMode::Repeating)),
            animation_timer: AnimationTimer(Timer::from_seconds(0.1, TimerMode::Repeating)), 
            animation_indices: AnimationIndices { first: 0, last: 9 },
            dir: Direction::Down,
            just_moved: JustMoved(false)                
        };
        commands.spawn((
            bum,
            Bum {},
            SpriteSheetBundle {
                texture: bum_texture,
                atlas: TextureAtlas {
                    layout: texture_atlas_layout.clone(),
                    index: 0
                },
                transform: Transform::from_scale(Vec3::splat(3.0)),
                ..default()
            }
        ));
    };
    
    for n in 0..3 {
        let mut transform = Transform::from_xyz(n as f32 * 5., 0., 0.);
        transform.scale = Vec3::splat(3.0);

        let thug_texture = asset_server.load("sprites/thug.png");
        
        let thug = Movement {
            movement_timer: MovementTimer(Timer::from_seconds(0.1, TimerMode::Repeating)),
            animation_timer: AnimationTimer(Timer::from_seconds(0.1, TimerMode::Repeating)), 
            animation_indices: AnimationIndices { first: 0, last: 9 },
            dir: Direction::Down,
            just_moved: JustMoved(false)                
        };
        commands.spawn((
            thug,
            Thug {},
            SpriteSheetBundle {
                texture: thug_texture,
                atlas: TextureAtlas {
                    layout: texture_atlas_layout.clone(),
                    index: 0
                },
                transform: Transform::from_scale(Vec3::splat(3.0)),
                ..default()
            }
        ));
    }
}

#[derive(QueryData)]
#[query_data(mutable)]
struct MoveableQuery {
    entity: Entity,
    movement_timer: &'static mut MovementTimer,
    just_moved: &'static mut JustMoved,
    dir: &'static mut Direction,
    transform: &'static mut Transform
}

fn animate_sprite2(
    time: Res<Time>,
    mut query: Query<(Entity, &mut AnimationTimer, &AnimationIndices, &Direction, &JustMoved, &mut TextureAtlas)>) {

    for (entity, mut animation_timer, animation_indices, dir, just_moved, mut atlas) in &mut query {
        if just_moved.0 {
            animation_timer.tick(time.delta());
            if animation_timer.just_finished() {
                atlas.index = if atlas.index % animation_indices.last == animation_indices.last - 1 {
                    dir.index() * animation_indices.last
                } else {
                    dir.index() * animation_indices.last + (atlas.index % animation_indices.last) + 1
                };
            }
        }
    }
}

fn update_bums(time: Res<Time>, mut query: Query<MoveableQuery, With<Bum>>) {
    for mut entity in &mut query {
        entity.movement_timer.tick(time.delta());       
        if entity.movement_timer.just_finished() {
            
            /* Decide whether to move left, right, up, down, or not to move at all.
            More weight should be placed on the decision to not move at all. When
            moving, we want to keep moving in that direction or stop moving. When
            stopped, we may switch directions.
             */
            if entity.just_moved.0 {
                // If the entity is moving, then there is a 70% chance it will continue moving
                let should_move = rand::random::<f32>();
                entity.just_moved.0 = should_move > 0.2;
            } else {
                // If the entity was not moving, then there is a 20% it will start moving
                let should_move = rand::random::<f32>();
                entity.just_moved.0 = should_move > 0.8;
                
                // Also allow it to change directions
                let new_dir_chance = rand::random::<bool>();
                if new_dir_chance {
                    let new_dir = rand::random::<u8>();
                    println!("New Dir: {}", new_dir);
                    if new_dir as f32 > 3. * u8::MAX as f32 / 4. {
                        println!("UP");
                        *entity.dir = Direction::Up;
                    } else if new_dir as f32 > 2. * u8::MAX as f32 / 4. {
                        println!("DOWN");
                        *entity.dir = Direction::Down;
                    } else if new_dir as f32 > u8::MAX as f32 / 4. {
                        println!("LEFT");
                        *entity.dir = Direction::Left;
                    } else {
                        println!("RIGHT");
                        *entity.dir = Direction::Right;
                    }
                }
            }
            
            if entity.just_moved.0 {
                if *entity.dir == Direction::Up {
                    entity.transform.translation.y += 5.;
                } else if *entity.dir == Direction::Down {
                    entity.transform.translation.y -= 5.;
                } else if *entity.dir == Direction::Left {
                    entity.transform.translation.x -= 5.;
                } else if *entity.dir == Direction::Right {
                    entity.transform.translation.x += 5.;
                }
            }
        }
    }
}

fn update_thugs(time: Res<Time>, mut query: Query<MoveableQuery, With<Thug>>) {
    for mut entity in &mut query {
        entity.movement_timer.tick(time.delta());       
        if entity.movement_timer.just_finished() {
            if entity.just_moved.0 {
                let should_move = rand::random::<f32>();
                entity.just_moved.0 = should_move > 0.05;
            } else {
                // If the entity was not moving, then there is a 20% it will start moving
                let should_move = rand::random::<f32>();
                entity.just_moved.0 = should_move > 0.5;
                
                // Also allow it to change directions
                let new_dir_chance = rand::random::<bool>();
                if new_dir_chance {
                    let new_dir = rand::random::<u8>();
                    println!("New Dir: {}", new_dir);
                    if new_dir as f32 > 3. * u8::MAX as f32 / 4. {
                        println!("UP");
                        *entity.dir = Direction::Up;
                    } else if new_dir as f32 > 2. * u8::MAX as f32 / 4. {
                        println!("DOWN");
                        *entity.dir = Direction::Down;
                    } else if new_dir as f32 > u8::MAX as f32 / 4. {
                        println!("LEFT");
                        *entity.dir = Direction::Left;
                    } else {
                        println!("RIGHT");
                        *entity.dir = Direction::Right;
                    }
                }
            }
            
            if entity.just_moved.0 {
                if *entity.dir == Direction::Up {
                    entity.transform.translation.y += 8.;
                } else if *entity.dir == Direction::Down {
                    entity.transform.translation.y -= 8.;
                } else if *entity.dir == Direction::Left {
                    entity.transform.translation.x -= 8.;
                } else if *entity.dir == Direction::Right {
                    entity.transform.translation.x += 8.;
                }
            }
        }
    }
}

fn update_cops(time: Res<Time>, mut query: Query<MoveableQuery, With<Cop>>) {
     for mut entity in &mut query {
        
    }
}

fn keyboard_input_system(
        time: Res<Time>, 
        mut query: Query<MoveableQuery, With<Player>>,
        keyboard_input: Res<ButtonInput<KeyCode>>) {
    
    for mut entity in &mut query {
            
        entity.movement_timer.tick(time.delta());

        if entity.movement_timer.just_finished() {

            let down = keyboard_input.pressed(KeyCode::ArrowDown);
            let up = keyboard_input.pressed(KeyCode::ArrowUp);
            let right = keyboard_input.pressed(KeyCode::ArrowRight);
            let left = keyboard_input.pressed(KeyCode::ArrowLeft);

            if left {
                *entity.dir = Direction::Left;
            } else if right {
                *entity.dir = Direction::Right;
            } else if down {
                *entity.dir = Direction::Down;
            } else if up {
                *entity.dir = Direction::Up;
            } 
                
            entity.transform.translation.y = entity.transform.translation.y + if keyboard_input.pressed(KeyCode::ArrowDown) {
                -5.
            } else if keyboard_input.pressed(KeyCode::ArrowUp) {
                5.
            } else { 0. };
            entity.transform.translation.x = entity.transform.translation.x + if keyboard_input.pressed(KeyCode::ArrowLeft) {
                -5.
            } else if keyboard_input.pressed(KeyCode::ArrowRight) {
                5.
            } else { 0. };
            
            entity.just_moved.0 = down || up || left || right;
        }
    }
}
